import requests
from bs4 import BeautifulSoup
import re

def fetch_exchange_rate():
    # Bing search URL for the exchange rate
    url = "https://www.bing.com/search?q=euro+to+inr"
    
    # User-Agent header to simulate a request from a web browser
    headers = {'User-Agent': 'Mozilla/5.0'}
    
    # Make the HTTP request to Bing
    response = requests.get(url, headers=headers)
    
    # Parse the HTML
    soup = BeautifulSoup(response.content, "html.parser")

    # Define the regular expression pattern to match the exchange rate
    pattern = re.compile(r"(\d+(\.\d+)?)\s*<strong>EUR</strong>\s*=\s*(\d+(\.\d+)?\s*)<strong>INR</strong>")

    # Initialize a variable to keep track of the found exchange rate
    exchange_rate = None

    # Search through the parsed HTML for the pattern
    for match in soup.find_all('p', class_='b_lineclamp4 b_algoSlug'):
        search = pattern.search(str(match))
        
        if search:
            # If a match is found, extract the exchange rate value
            exchange_rate = search.group(0)
            
            break
    matches = re.findall(r'(\d+\.\d+)\s*<strong>(EUR|INR)</strong>', exchange_rate)
    # Extract and print the values
    eur_value, inr_value = 0, 0
    for value, currency in matches:
        if currency == 'EUR':
            eur_value = float(value)
        elif currency == 'INR':
            inr_value = float(value)
    print("Euro: " + str(eur_value))
    print("INR: " + str(inr_value))

fetch_exchange_rate()
